from flask import render_template, request, jsonify, session
from app.models import Logs, Users

from . import process

from app import db
from app.tests import test_case

@process.route('/logs')
def render_logs():
    logslist = db.session.query(Logs,Users).filter(Logs.id_user_fk_logs==Users.id_user).all()
    context = {
        'logslist': logslist
    }
    return render_template('logs.html', **context)

@process.route('/process')
def render_proces():
    session['username'] = 'julio'
    if session:
        return render_template('process.html')
    else:
        return "404", 404

@process.route('/registerActionProcess', methods=['POST'])
def register_action_process():
    #I made this way for test units. I can use the global object g.id_user
    idUser=request.form['idUser']
    new_log = Logs(action=request.form['action'], id_user_fk_logs=idUser)
    db.session.add(new_log)
    db.session.commit()
    return jsonify("The log has been register successfully!."), 201
    
@process.route('/about')
def about():
    return render_template('about.html')