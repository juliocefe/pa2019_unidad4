// self executing function here
(function() {
    // your page initialization code here
    // the DOM will be available here
    document.getElementById('aceptDeleteUser').addEventListener('click', () => {
        event.preventDefault();
        var id = document.getElementById('idField').value
        fetch(`/users/deleteUser/${id}`, {
            method: 'DELETE',
            credentials: "same-origin"
        })
        .then(
          function(response) {  
            if (response.status !== 200) {
              console.log('Looks like there was a problem. Status Code: ' +
                response.status);
              return;
            }
            // Examine the text in the response
            response.json().then(function(data) {
                window.location = '/users/users'
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
      }
    )

 })();

 openModal = (id) => {
    document.getElementById('idField').value = id
    let username = document.getElementById(`${id}username`).innerText
    let role = document.getElementById(`${id}role`).innerText
    document.getElementById('title').innerHTML = `Are you shure about delete the ${role}: `
    document.getElementById('title2').innerHTML = `"${username}" ?`
    document.getElementById('myModalDeleter').style.display = 'flex'
 }

closeModal = () => {
    document.getElementById('idField').value = 0
    document.getElementById('myModalDeleter').style.display  = 'none'
 }