// self executing function here
(function() {
    // your page initialization code here
    // the DOM will be available here

    // The best watch I could do
    document.getElementById('hour').innerHTML = moment().format("ddd MMM YYYY H:mm:ss")
    setInterval(update, 1000);
                                                            // The best watch I could do

    var buttonslist = document.getElementsByClassName('btn-process')
    var motor = document.getElementById('formBox')
    for (let index = 0; index < buttonslist.length; index++) {
        buttonslist[index].addEventListener('click', (e) => {
        // This is for rotate the motor when the "turn right" button is clicked
            let button = e.target
            if(e.target.value === 'turn right'){
                motor.classList.add('rotate')
                button.disabled = true
                setTimeout(() => {
                    motor.classList.remove('rotate')
                    button.disabled = false
                }, 1000);
            }
            event.preventDefault();
            var form = new FormData();
            form.append('action', e.target.value);
            form.append('idUser', document.getElementById('idUser').value);
            fetch('/registerActionProcess', {
                method: 'POST',
                credentials: "same-origin",
                body: form
            })
            .then(function(response) {  
                if (response.status !== 201) {
                    console.log(`Looks like there was a problem. Status Code: ${response.status}`);
                    return;
                }
                // Examine the text in the response
                response.json().then(function(data) {
                    console.log('actioned')
                });
            }).catch(function(err) {
                console.log('Fetch Error :-S', err);
            });
        })
    }

 })();

function update(){
    document.getElementById('hour').innerHTML = moment().format("ddd MMM YYYY H:mm:ss")
}